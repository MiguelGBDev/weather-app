import { WEATHER_TYPE } from 'src/weather/entities/weather.enum'

export interface Stat {
  cityName: string
  currentTemp: number
  averageTodayTemp: number
  forecastTemp: number
  currentWeather: WEATHER_TYPE
  forecastWeather: WEATHER_TYPE
  lastRecordDate: Date
}
