import { Injectable } from '@nestjs/common'
import { CityService } from 'src/city/city.service'
import { TemperatureService } from 'src/temperature/temperature.service'
import { WeatherService } from 'src/weather/weather.service'
import { Stat } from './entities/stat'

@Injectable()
export class StatService {
  constructor(
    private readonly cityService: CityService,
    private readonly temperatureService: TemperatureService,
    private readonly weatherTypeService: WeatherService
  ) {}

  async getStats(cityName: string): Promise<Stat> {
    const { id: cityId } = await this.cityService.getByName(cityName)

    const [{ currentTemp, averageTodayTemp, forecastTemp, lastRecordDate }, { currentWeather, forecastWeather }] =
      await Promise.all([this.temperatureService.getStats(cityId), this.weatherTypeService.getStats(cityId)])

    return { cityName, currentTemp, averageTodayTemp, forecastTemp, currentWeather, forecastWeather, lastRecordDate }
  }
}
