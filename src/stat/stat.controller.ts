import { Controller, Get, HttpException, HttpStatus, Param } from '@nestjs/common'
import { StatService } from './stat.service'

@Controller('weather')
export class StatController {
  constructor(private readonly statService: StatService) {}

  @Get(':cityName')
  async findOne(@Param('cityName') cityName: string) {
    try {
      return await this.statService.getStats(cityName)
    } catch (e) {
      if (e.message === 'Not found') {
        throw new HttpException(e.message, HttpStatus.NOT_FOUND)
      }
    }
  }
}
