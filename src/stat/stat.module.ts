import { Module } from '@nestjs/common'
import { StatService } from './stat.service'
import { StatController } from './stat.controller'
import { CityService } from 'src/city/city.service'
import { CityRepository } from 'src/city/city.repository'
import { PrismaService } from 'src/prisma/prisma.service'
import { TemperatureService } from 'src/temperature/temperature.service'
import { TemperatureRepository } from 'src/temperature/temperature.repository'
import { WeatherService } from 'src/weather/weather.service'
import { WeatherRepository } from 'src/weather/weather.repository'

@Module({
  controllers: [StatController],
  providers: [
    StatService,
    CityService,
    CityRepository,
    PrismaService,
    TemperatureService,
    TemperatureRepository,
    WeatherService,
    WeatherRepository
  ]
})
export class StatModule {}
