import { Injectable } from '@nestjs/common'
import { subDays } from 'date-fns'
import { PrismaService } from 'src/prisma/prisma.service'
import { TempDto } from 'src/utils/temperature'

@Injectable()
export class TemperatureRepository {
  constructor(private readonly prisma: PrismaService) {}

  async getLastByCityId(cityId: string) {
    const response = await this.prisma.temperature.findMany({
      where: { cityId },
      orderBy: {
        createdAt: 'desc'
      },
      take: 1
    })

    return response[0]
  }

  async getByCityIdAndDate(cityId: string, date: Date) {
    return await this.prisma.temperature.findFirst({
      where: { cityId, createdAt: date }
    })
  }

  async createMany(temps: TempDto[]) {
    await this.prisma.temperature.createMany({ data: temps })
  }

  /**
   * This method return, giving a city id and a date as parameter,
   * the average for every temperature value
   * between the time passed as parameter and the previous date.
   * As this way we can have the median for a period of 24 hours.
   */
  async getAverageByCityIdAndDate(cityId: string, date: Date): Promise<number | undefined> {
    const lastDayDate = subDays(date, 1)

    const response = await this.prisma.temperature.aggregate({
      _avg: {
        value: true
      },
      where: {
        AND: [
          {
            cityId
          },
          {
            createdAt: {
              gte: lastDayDate,
              lte: date
            }
          }
        ]
      }
    })

    return response._avg?.value
  }
}
