import { Injectable } from '@nestjs/common'
import { TemperatureRepository } from './temperature.repository'
import { addDays, subYears } from 'date-fns'

@Injectable()
export class TemperatureService {
  constructor(private readonly temperatureRepository: TemperatureRepository) {}

  async getStats(
    cityId: string
  ): Promise<{ currentTemp: number; averageTodayTemp: number; forecastTemp: number; lastRecordDate: Date }> {
    const lastTempDto = await this.temperatureRepository.getLastByCityId(cityId)

    if (!lastTempDto) {
      throw new Error('Empty data')
    }

    const { averageTodayTemp, forecastTemp } = await this.getForecast(cityId, lastTempDto.createdAt)

    return { currentTemp: lastTempDto.value, averageTodayTemp, forecastTemp, lastRecordDate: lastTempDto.createdAt }
  }

  /**
   * Private method that return the forecast of temperature for the next day
   *
   * Forecast is calculated by the media of every entry for all the temperatures of today and the next day of the last year.
   * The forecast include one decimal trying to give a more accurate value
   */
  private async getForecast(cityId: string, date: Date): Promise<{ averageTodayTemp: number; forecastTemp: number }> {
    const averageTodayTemp = await this.temperatureRepository.getAverageByCityIdAndDate(cityId, date)

    const lastYear = subYears(date, 1)
    const nextDayLastYear = addDays(lastYear, 1)

    const averageTempNextDayLastYear = await this.temperatureRepository.getAverageByCityIdAndDate(
      cityId,
      nextDayLastYear
    )

    const averageTemp = (averageTodayTemp + averageTempNextDayLastYear) / 2
    const forecastTemp = Math.round(averageTemp * 10) / 10

    return { averageTodayTemp: Math.round(averageTodayTemp * 10) / 10, forecastTemp }
  }
}
