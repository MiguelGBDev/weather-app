import { Injectable } from '@nestjs/common'
import { addDays, subYears } from 'date-fns'
import { WeatherRepository } from './weather.repository'
import { WEATHER_TYPE } from './entities/weather.enum'

@Injectable()
export class WeatherService {
  constructor(private readonly weatherRepository: WeatherRepository) {}

  async getStats(cityId: string): Promise<{
    currentWeather: WEATHER_TYPE
    forecastWeather: WEATHER_TYPE
    lastRecordDate: Date
  }> {
    const lastWeatherDto = await this.weatherRepository.getLastByCityId(cityId)

    if (!lastWeatherDto) {
      throw new Error('Empty data')
    }

    const forecastWeather = await this.getForecast(cityId, lastWeatherDto.createdAt)

    return {
      currentWeather: lastWeatherDto.type as WEATHER_TYPE,
      forecastWeather,
      lastRecordDate: lastWeatherDto.createdAt
    }
  }

  /**
   * Private method that return the forecast of weather for the next day
   *
   * Forecast is calculated giving every entry for every weather type of today and the next day of the last year.
   * Having the count of every weather type, we defined the weather forecast as the max of the weather types that appeared.
   */
  private async getForecast(cityId: string, date: Date): Promise<WEATHER_TYPE> {
    const lastYear = subYears(date, 1)
    const nextDayLastYear = addDays(lastYear, 1)

    const [entriesToday, entriesNextDayLastYear] = await Promise.all([
      this.weatherRepository.getEntriesByCityIdAndDate(cityId, date),
      this.weatherRepository.getEntriesByCityIdAndDate(cityId, nextDayLastYear)
    ])

    const result = Object.values(WEATHER_TYPE).map(elem => {
      return { type: elem, count: entriesToday[elem] + entriesNextDayLastYear[elem] }
    })

    const maxCountWeather = result.reduce((prev, current) => (prev.count > current.count ? prev : current))

    return maxCountWeather.type
  }
}
