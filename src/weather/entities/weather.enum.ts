export enum WEATHER_TYPE {
  SUNNY = 'sunny',
  PARTIALLY_CLOUDY = 'partially_cloudy',
  CLOUDY = 'cloudy',
  RAINY = 'rainy',
  STORMY = 'stormy',
  SNOWY = 'snowy'
}
