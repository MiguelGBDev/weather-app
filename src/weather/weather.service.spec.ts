import { WeatherService } from './weather.service'
import { WeatherRepository } from './weather.repository'
import { PrismaService } from '../prisma/prisma.service'
import { WEATHER_TYPE } from './entities/weather.enum'

const BAGHDAG_ID = 'BaghDagId'

describe('WeatherService', () => {
  const repo = new WeatherRepository({} as PrismaService)
  const service = new WeatherService(repo)

  beforeEach(async () => {
    jest.spyOn(repo, 'getLastByCityId').mockImplementation(async cityId => {
      if (cityId === BAGHDAG_ID) {
        return {
          cityId: BAGHDAG_ID,
          type: WEATHER_TYPE.CLOUDY,
          createdAt: new Date()
        }
      } else return undefined
    })

    jest.spyOn(repo, 'getEntriesByCityIdAndDate').mockImplementation(async () => {
      return {
        sunny: 0,
        partially_cloudy: 100,
        cloudy: 200,
        rainy: 100,
        stormy: 50,
        snowy: 0
      }
    })
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('gestStats', () => {
    it('should return the desired stats', async () => {
      const resp = await service.getStats(BAGHDAG_ID)

      expect(resp).toBeDefined()
      expect(resp.currentWeather).toBe(WEATHER_TYPE.CLOUDY)
      expect(resp.forecastWeather).toBe(WEATHER_TYPE.CLOUDY)
      expect(resp.lastRecordDate).toBeInstanceOf(Date)
    })

    it('should throw error if city does not exists', async () => {
      let err
      try {
        await service.getStats('wrongId')
      } catch (error) {
        err = error
      } finally {
        expect(err).toBeDefined()
        expect(err.message).toBe('Empty data')
      }
    })
  })

  describe('getForecast', () => {
    it('should return the desired stats', async () => {
      const resp = await service.getStats(BAGHDAG_ID)

      expect(resp).toBeDefined()
      expect(resp.currentWeather).toBe(WEATHER_TYPE.CLOUDY)
      expect(resp.forecastWeather).toBe(WEATHER_TYPE.CLOUDY)
      expect(resp.lastRecordDate).toBeInstanceOf(Date)
    })

    it('should throw error if city does not exists', async () => {
      let err
      try {
        await service.getStats('wrongId')
      } catch (error) {
        err = error
      } finally {
        expect(err).toBeDefined()
        expect(err.message).toBe('Empty data')
      }
    })
  })
})
