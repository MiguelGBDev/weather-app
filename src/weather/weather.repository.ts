import { Injectable } from '@nestjs/common'
import { subDays } from 'date-fns'
import { PrismaService } from '../prisma/prisma.service'
import { WEATHER_TYPE } from './entities/weather.enum'
import { WeatherDto } from 'src/utils/weather'

@Injectable()
export class WeatherRepository {
  constructor(private readonly prisma: PrismaService) {}

  async getLastByCityId(cityId: string) {
    const response = await this.prisma.weather.findMany({
      where: { cityId },
      orderBy: {
        createdAt: 'desc'
      },
      take: 1
    })

    const {
      0: { type, createdAt }
    } = response
    const weather = { cityId, type: type as WEATHER_TYPE, createdAt }
    return weather
  }

  async getByCityIdAndDate(cityId: string, date: Date) {
    const response = await this.prisma.weather.findFirst({
      where: { cityId, createdAt: date }
    })

    return { cityId, type: response.type as WEATHER_TYPE, createdAt: response.createdAt }
  }

  async createMany(temps: WeatherDto[]) {
    await this.prisma.weather.createMany({ data: temps })
  }

  /**
   * This method return, giving a city id and a date as parameter,
   * a dictionary with all posible values for weather and the number of rows appeared
   * between the time passed as parameter and the previous date.
   * As this way we can have all the values for a period of 24 hours.
   */
  async getEntriesByCityIdAndDate(cityId: string, date: Date): Promise<Record<WEATHER_TYPE, number>> {
    const lastDayDate = subDays(date, 1)

    const response = await this.prisma.weather.groupBy({
      by: ['type'],
      _count: {
        type: true
      },
      where: {
        AND: [
          {
            cityId
          },
          {
            createdAt: {
              gte: lastDayDate,
              lte: date
            }
          }
        ]
      }
    })

    const countByType: Record<WEATHER_TYPE, number> = {} as Record<WEATHER_TYPE, number>
    Object.values(WEATHER_TYPE).forEach(type => {
      const count = response.find(elem => elem.type === type)?._count.type
      countByType[type] = count ?? 0
    })

    return countByType
  }
}
