import { Injectable } from '@nestjs/common'

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hi from WeatherApp! You are welcome!'
  }
}
