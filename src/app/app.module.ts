import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { PrismaModule } from '../prisma/prisma.module'
import { AppService } from './app.service'
import { AppController } from './app.controller'
import { TaskService } from '../task/task.service'
import { StatModule } from 'src/stat/stat.module'
import { CityRepository } from 'src/city/city.repository'
import { TemperatureRepository } from 'src/temperature/temperature.repository'
import { WeatherRepository } from 'src/weather/weather.repository'

@Module({
  imports: [PrismaModule, ScheduleModule.forRoot(), StatModule],
  controllers: [AppController],
  providers: [AppService, TaskService, CityRepository, TemperatureRepository, WeatherRepository]
})
export class AppModule {}
