import { Injectable } from '@nestjs/common'
import { CityRepository } from './city.repository'

@Injectable()
export class CityService {
  constructor(private readonly cityRepository: CityRepository) {}

  async getByName(name: string) {
    const cityDto = await this.cityRepository.getCityByName(name)

    if (!cityDto) {
      throw new Error('Not found')
    }

    return cityDto
  }
}
