import { WEATHER_TYPE } from 'src/weather/entities/weather.enum'

export interface City {
  rangeWeather: WEATHER_TYPE[]
  mediaWeather: WEATHER_TYPE
  minTemp: number
  maxTemp: number
  mediaTemp: number
  name: string
}
