import { Injectable } from '@nestjs/common'
import { PrismaService } from 'src/prisma/prisma.service'

@Injectable()
export class CityRepository {
  constructor(private readonly prisma: PrismaService) {}

  async getCityByName(name: string) {
    return await this.prisma.city.findFirst({ where: { name } })
  }

  async getAll() {
    return await this.prisma.city.findMany({})
  }
}
