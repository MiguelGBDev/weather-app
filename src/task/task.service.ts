import { Injectable } from '@nestjs/common'
import { Cron } from '@nestjs/schedule'
import { addDays, addHours, addSeconds, subYears } from 'date-fns'
import * as dotenv from 'dotenv'

import { CityRepository } from 'src/city/city.repository'
import { TemperatureRepository } from 'src/temperature/temperature.repository'
import { WeatherRepository } from 'src/weather/weather.repository'

import { cityMocks } from '../utils/entities/cityMocks'
import { createTemperatureValue } from 'src/utils/temperature'
import { createWeatherType } from 'src/utils/weather'

dotenv.config()
const PERIOD_TEMP_SECONDS = Number(process.env.PERIOD_TEMP_SECONDS) || 5
const PERIOD_WEATHER_HOURS = Number(process.env.PERIOD_WEATHER_HOURS) || 2
const PERIOD = `*/${PERIOD_TEMP_SECONDS} * * * * *`

@Injectable()
export class TaskService {
  weatherTimer: Date

  constructor(
    private readonly cityRepository: CityRepository,
    private readonly temperatureRepository: TemperatureRepository,
    private readonly weatherRepository: WeatherRepository
  ) {}

  @Cron(PERIOD)
  async handleCron() {
    const newTemps = []
    const newWeathers = []
    let changedWeather = false

    for (const cityMock of cityMocks) {
      const { minTemp, maxTemp, rangeWeather } = cityMock

      const { id: cityId } = await this.cityRepository.getCityByName(cityMock.name)

      const [lastTemp, lastWeather] = await Promise.all([
        this.temperatureRepository.getLastByCityId(cityId),
        this.weatherRepository.getLastByCityId(cityId)
      ])

      if (!this.weatherTimer) {
        this.weatherTimer = lastTemp.createdAt
      }

      const lastYearDate = subYears(lastTemp.createdAt, 1)
      const lastYearNextDayDate = addDays(lastYearDate, 1)

      const [lastYearTemp, lastYearWeather] = await Promise.all([
        this.temperatureRepository.getByCityIdAndDate(cityId, lastYearNextDayDate),
        this.weatherRepository.getByCityIdAndDate(cityId, lastYearNextDayDate)
      ])

      const newTemp = createTemperatureValue(lastTemp.value, minTemp, maxTemp)
      const newLastYearTemp = createTemperatureValue(lastYearTemp.value, minTemp, maxTemp)

      newTemps.push(
        { value: newTemp, cityId, createdAt: addSeconds(lastTemp.createdAt, PERIOD_TEMP_SECONDS) },
        { value: newLastYearTemp, cityId, createdAt: addSeconds(lastYearNextDayDate, PERIOD_TEMP_SECONDS) }
      )

      if (lastTemp.createdAt.getTime() === this.weatherTimer.getTime()) {
        changedWeather = true
      }

      const newWeather = changedWeather ? createWeatherType(lastWeather.type, rangeWeather) : lastWeather.type
      const newLastYearWeather = changedWeather
        ? createWeatherType(lastYearWeather.type, rangeWeather)
        : lastYearWeather.type

      newWeathers.push(
        { type: newWeather, cityId, createdAt: addSeconds(lastTemp.createdAt, PERIOD_TEMP_SECONDS) },
        { type: newLastYearWeather, cityId, createdAt: addSeconds(lastYearNextDayDate, PERIOD_TEMP_SECONDS) }
      )
    }

    await Promise.all([this.temperatureRepository.createMany(newTemps), this.weatherRepository.createMany(newWeathers)])

    console.info('New mocked data inserted. Datetime: ', new Date())

    if (changedWeather) {
      this.weatherTimer = addHours(this.weatherTimer, PERIOD_WEATHER_HOURS)
      changedWeather = false
    }
  }
}
