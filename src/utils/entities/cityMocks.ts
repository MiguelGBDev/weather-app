import { City } from 'src/city/entities/city.entity.seed'
import { WEATHER_TYPE } from '../../weather/entities/weather.enum'

const BAGHDAG_CITY: City = {
  rangeWeather: [WEATHER_TYPE.SUNNY, WEATHER_TYPE.PARTIALLY_CLOUDY, WEATHER_TYPE.CLOUDY, WEATHER_TYPE.RAINY],
  mediaWeather: WEATHER_TYPE.PARTIALLY_CLOUDY,
  minTemp: 23,
  maxTemp: 29,
  mediaTemp: 26,
  name: 'Baghdag'
}

const CACERES_CITY: City = {
  rangeWeather: [WEATHER_TYPE.PARTIALLY_CLOUDY, WEATHER_TYPE.CLOUDY, WEATHER_TYPE.RAINY, WEATHER_TYPE.STORMY],
  mediaWeather: WEATHER_TYPE.CLOUDY,
  minTemp: 17,
  maxTemp: 23,
  mediaTemp: 20,
  name: 'Cáceres'
}

const STOCKHOLM_CITY: City = {
  rangeWeather: [WEATHER_TYPE.CLOUDY, WEATHER_TYPE.RAINY, WEATHER_TYPE.STORMY, WEATHER_TYPE.SNOWY],
  mediaWeather: WEATHER_TYPE.RAINY,
  minTemp: 11,
  maxTemp: 17,
  mediaTemp: 14,
  name: 'Stockholm'
}

export const cityMocks: City[] = [BAGHDAG_CITY, CACERES_CITY, STOCKHOLM_CITY]
