export interface TempDto {
  value: number
  cityId: string
  createdAt: Date
}
