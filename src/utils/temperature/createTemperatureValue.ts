import { getRandomModifier } from '../getRandomModifier'

export const createTemperatureValue = (previousTemp: number, minTemp: number, maxTemp: number): number => {
  // To make temps more realistic, values ​​oscillate according to the previous one and only can differ in one grade.
  // modifier value could be 1, 0 or -1
  let newTemp = previousTemp + getRandomModifier()

  // Force temps to stay in the range we set
  newTemp = newTemp < minTemp ? minTemp : newTemp
  newTemp = newTemp > maxTemp ? maxTemp : newTemp

  return newTemp
}
