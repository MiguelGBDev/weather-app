import { createTemperatureValue } from './createTemperatureValue'
import { TempDto } from './entities/temperatureDto'

export { TempDto, createTemperatureValue }
