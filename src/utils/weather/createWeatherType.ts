import { WEATHER_TYPE } from '../../weather/entities/weather.enum'
import { getRandomModifier } from '../getRandomModifier'

export const createWeatherType = (previousWeather: WEATHER_TYPE, rangeWeather: WEATHER_TYPE[]): WEATHER_TYPE => {
  const index = Object.values(WEATHER_TYPE).indexOf(previousWeather)

  const newIndex = index + getRandomModifier()
  let newWeather = Object.values(WEATHER_TYPE)[newIndex]

  newWeather = Object.values(rangeWeather).includes(newWeather) ? newWeather : previousWeather

  return newWeather
}
