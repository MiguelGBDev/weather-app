import { WEATHER_TYPE } from 'src/weather/entities/weather.enum'

export interface WeatherDto {
  type: WEATHER_TYPE
  cityId: string
  createdAt: Date
}
