/*
  Warnings:

  - The values [STORM] on the enum `Type` will be removed. If these variants are still used in the database, this will fail.
  - Added the required column `cityId` to the `Weather` table without a default value. This is not possible if the table is not empty.
  - Added the required column `temp` to the `Weather` table without a default value. This is not possible if the table is not empty.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "Type_new" AS ENUM ('SUNNY', 'PARTIALLY_CLOUDY', 'CLOUDY', 'RAINY', 'STORMY', 'SNOWY');
ALTER TABLE "Weather" ALTER COLUMN "type" DROP DEFAULT;
ALTER TABLE "Weather" ALTER COLUMN "type" TYPE "Type_new" USING ("type"::text::"Type_new");
ALTER TYPE "Type" RENAME TO "Type_old";
ALTER TYPE "Type_new" RENAME TO "Type";
DROP TYPE "Type_old";
COMMIT;

-- AlterTable
ALTER TABLE "Weather" ADD COLUMN     "cityId" TEXT NOT NULL,
ADD COLUMN     "temp" INTEGER NOT NULL,
ALTER COLUMN "type" DROP DEFAULT;

-- CreateTable
CREATE TABLE "City" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "City_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Weather" ADD CONSTRAINT "Weather_cityId_fkey" FOREIGN KEY ("cityId") REFERENCES "City"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
