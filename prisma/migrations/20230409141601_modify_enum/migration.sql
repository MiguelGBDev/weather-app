/*
  Warnings:

  - The values [SUNNY,PARTIALLY_CLOUDY,CLOUDY,RAINY,STORMY,SNOWY] on the enum `Type` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "Type_new" AS ENUM ('sunny', 'partially_cloudy', 'cloudy', 'rainy', 'stormy', 'snow');
ALTER TABLE "Weather" ALTER COLUMN "type" TYPE "Type_new" USING ("type"::text::"Type_new");
ALTER TYPE "Type" RENAME TO "Type_old";
ALTER TYPE "Type_new" RENAME TO "Type";
DROP TYPE "Type_old";
COMMIT;
