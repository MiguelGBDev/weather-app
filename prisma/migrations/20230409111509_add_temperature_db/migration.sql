/*
  Warnings:

  - You are about to drop the column `createdAt` on the `City` table. All the data in the column will be lost.
  - You are about to drop the column `updatedAt` on the `City` table. All the data in the column will be lost.
  - You are about to drop the column `temp` on the `Weather` table. All the data in the column will be lost.
  - You are about to drop the column `updatedAt` on the `Weather` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "City" DROP COLUMN "createdAt",
DROP COLUMN "updatedAt";

-- AlterTable
ALTER TABLE "Weather" DROP COLUMN "temp",
DROP COLUMN "updatedAt";

-- CreateTable
CREATE TABLE "Temperature" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "value" INTEGER NOT NULL,
    "cityId" TEXT NOT NULL,

    CONSTRAINT "Temperature_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Temperature" ADD CONSTRAINT "Temperature_cityId_fkey" FOREIGN KEY ("cityId") REFERENCES "City"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
