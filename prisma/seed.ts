import { PrismaClient } from '@prisma/client'
import { subDays, subHours, addSeconds, subYears, addDays } from 'date-fns'
import * as dotenv from 'dotenv'

import { createTemperatureValue, TempDto } from '../src/utils/temperature'
import { WeatherDto, createWeatherType } from '../src/utils/weather'

import { cityMocks } from '../src/utils/entities/cityMocks'

dotenv.config()

const PERIOD_TEMP_SECONDS = Number(process.env.PERIOD_TEMP_SECONDS) || 5
const PERIOD_WEATHER_HOURS = Number(process.env.PERIOD_WEATHER_HOURS) || 1

const prisma = new PrismaClient()

async function main() {
  const CURRENT_DATE = new Date()

  for (const city of cityMocks) {
    const { id: cityId } = await prisma.city.create({ data: { name: city.name } })

    let timeIterator = subDays(CURRENT_DATE, 1)
    let lastYearTimeIterator = subYears(timeIterator, 1)
    lastYearTimeIterator = addDays(lastYearTimeIterator, 1)
    let weatherPeriods = 24

    const arrayTemp: TempDto[] = []
    let previousTemp = city.mediaTemp
    let previousLastYearTemp = city.mediaTemp
    const { minTemp, maxTemp } = city

    const arrayWeather: WeatherDto[] = []
    let previousWeather = city.mediaWeather
    let previousLastYearWeather = city.mediaWeather

    const { rangeWeather } = city

    while (timeIterator < CURRENT_DATE) {
      //  Set random Temperatures
      previousTemp = createTemperatureValue(previousTemp, minTemp, maxTemp)
      previousLastYearTemp = createTemperatureValue(previousLastYearTemp, minTemp, maxTemp)

      arrayTemp.push(
        { value: previousTemp, cityId, createdAt: timeIterator },
        { value: previousLastYearTemp, cityId, createdAt: lastYearTimeIterator }
      )

      // change random weather type for defined periods
      if (timeIterator.getTime() === subHours(CURRENT_DATE, weatherPeriods).getTime()) {
        previousWeather = createWeatherType(previousWeather, rangeWeather)
        previousLastYearWeather = createWeatherType(previousLastYearWeather, rangeWeather)

        weatherPeriods -= PERIOD_WEATHER_HOURS
      }

      arrayWeather.push(
        { type: previousWeather, cityId, createdAt: timeIterator },
        { type: previousLastYearWeather, cityId, createdAt: lastYearTimeIterator }
      )

      timeIterator = addSeconds(timeIterator, PERIOD_TEMP_SECONDS)
      lastYearTimeIterator = addSeconds(lastYearTimeIterator, PERIOD_TEMP_SECONDS)
    }

    await Promise.all([
      prisma.temperature.createMany({ data: arrayTemp }),
      prisma.weather.createMany({ data: arrayWeather })
    ])
  }
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
