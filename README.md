## Proposal

Create a REST API with 1 Endpoint. It shows the current Weather of the city of your choice and the forecast for the next few days.
Please also provide the average Temperature during that timeframe.

To provide Data within your API you must use mocked data. Hint: create mock data for 3 cities.

## State of art

The proposal is implemented using some modern and advanced technologies to face the real problem.

- There are three cities with different temperatures and weather types predefined: Baghdag, Caceres, and Stockholm.
- Baghdag has a high-temperature range and hot weather types (sunny, partially cloudy, cloudy, and rainy).
- Caceres has medium temperatures and weather types in the middle (partially cloudy, cloudy, rainy, and stormy).
- Stockholm will have low temperatures and more extreme climatic types (cloudy, rainy, stormy, and snowy).
- Temperatures and weather data are semi-random mocked and inserted into the database through a seed script.
- A cronjob script is added to generate new semi-random data for predefined periods.
- A forecasting approach is developed using the average from the previous day's data and the same day's data from the last year for temperatures.
- Weather forecasting gets the most appeared weather types from the same periods than temperature.
- Stats are provided outside by an HTTP endpoint.

![IMAGE_DESCRIPTION](flows.png)

## Description

## Technologies applied

- [PostgreSQL](https://www.postgresql.org/)
- [Docker](https://www.docker.com/) & [Docker desktop](https://www.docker.com/products/docker-desktop/)
- [Prisma ORM](https://www.prisma.io/)
- [Nodejs](https://nodejs.org/en)
- [Nestjs framework](https://github.com/nestjs/nest)
- [Postman](https://www.postman.com/)
- [DBeaver](https://dbeaver.io/)
- [Typescript](https://www.typescriptlang.org/)

## Nest features

- Modules
- Services
- Controllers
- ScheduleModule

## Prisma features

- Migrations (Create the full structure of the database in an iterative way)
- Seeds (Populate the database with initial data)

## Tech implementation

### Why Nestjs?

Nestjs framework provides highly advanced features and design patterns in an easy way.
It provides a handy way to use dependency injection with a container design pattern.
It uses decorator features.

The main reason why I choose Nest to make this project is I consider it a good opportunity to learn about it and test if it speedup up the developing (IMHO, it does!).

### Why Prisma?

Prisma ORM provides protection to SQL Injection, and an easy middleware to make queries in a programmatic way.
It provides as well a useful database seed.
Prisma also includes migrations feature to create/modify the final database structure in an incremental mode, a very handy way to recover from errors in structure definition.

### Database structure

We have three entities: City, Temperature, and Weather.

City entities in on the top, and Temperature and Weather are related to them. It doesn't makes sense if they are not linked to an existing city.

![IMAGE_DESCRIPTION](database.png)

### Service arquitecture

The architecture of the API follows the DDD concepts for domain entities, use cases and business logic isolated.
There is an implementation for the design pattern repository to create an extra layer to communicate with the database to ingest or receive data.

So, in the end, we have a controller for the desired endpoint, followed by a service that coordinates the flows, three isolated services for the entities (city, temperature, and weather), and three repositories.
An extra isolated service is implemented to ingest data periodically.

![IMAGE_DESCRIPTION](architecture.png)

## Explanations and comments

### Why data are semi-random?

The mocked values for temp and weather type aren't totally random. I decided to implement it in this way to make the result more realistic.
It makes nonsense, for instance, to snow and the next row be sunny.
So the mechanism to generate mock data reads the previous row, and the next row only can move to throw the values immediately up and down.

If the previous temperature row values 22, the next one only could have the same value, one degree more, or one degree less.

Same for weather types:
SUNNY
PARTIALLY_CLOUDY
CLOUDY
RAINY
STORMY
SNOWY

If the previous weather type is RAINY, the next one could be CLOUDY, RAINY, or STORMY.

The temperatures can variate for every period, based in seconds (5 seconds by default),
but weather type only will change according to an extra environment variable, based on hours (2 hours by default)

### Seed script

The initial data ingest reads environment variables (stored in .env) PERIOD_TEMP_SECONDS, PERIOD_WEATHER_HOURS, and loops times increasing them as periods.
Then, inside each loop, it generates new mock data for temp and weather for the next period, coming from the previous values.

The algorithm inserts data for the previous 24 hours, used for averages and for the next day for the previous year, to calculate the forecast.
It makes the same flow for the three predefined cities.

Once the loops are finished, all the generated data will be sent to the database to store them.

### Task cronjob

There is an additional script that creates new values as time passes.
The script reads the same environment variables, so the periods for the new data will be the same as the seed that has been generated.

It will be the same mechanism as the seed, creating a value for the current moment, and another one for the same moment in the previous year the next day.
At this way, the result of calling the endpoint can be modified over time.

## Requirements

- npm
- nodejs
- docker

Open a terminal and type:

```bash
$ npm install
```

With this command all interal dependencies for the project will be added.

## Prepare the environment

### Build database cointainer

First, we will create our database in a docker container and deploying it:

```bash
$ nmp run infra:up
```

### Create database structure and insert initial data

The second and the third step is to create the data structure and fill the DB with the initial mocked data. We will accomplish it with a single command:

```bash
$ npm run db:reset
```

CAUTION: If you runned it previously and you use it another time, previous data will be removed!

## Running the app

Use any of the commands below to start the app (dev command is recommended):

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

With these commands, the app starts to listen to requests from the outside. The cronjob for mock data ingest should start running as well.

### Check results

Import the Postman collection include it in the source folder in a Postman client and send the predefined requests to check the responses.

Alternatively, you can use the following command via shell:

```
curl --location 'localhost:3000/weather/Cáceres'
```

This request will ask for the data for Cáceres City.

## Test

The testing tool I used is Jest.

There are some light tests included to show how I use mocks, spies, and Jest features.
I preferred to invest more time in the project arquitecture, so I didn't implement a full suite of test.

To run the test suite:

```bash
$ npm test
```
